#!/usr/bin/env swift

// ActualForceQuit.swift
// by johsoderi
// Tested on Big Sur 11.4 w/ Swift v5.3.2

import AppKit
import Cocoa
import Foundation

// SETTINGS
let version = "v1.13 Sept 2021"
let windowTitle = "ActualForceQuit"
let windowBGColor = NSColor.black
let buttonTextFont = NSFont(name: "Menlo-Regular", size: 14)
let buttonTextColor = NSColor.systemOrange
let normalFont = NSFont(name: "Menlo-Regular", size: 13.0)
let boldFont = NSFont(name: "Menlo-Bold", size: 13.0)
let bigFont = NSFont(name: "Menlo-Bold", size: 14.5)
let smallFont = NSFont(name: "Menlo-Regular", size: 10.0)
let textFGColor = NSColor.systemOrange
let textBGColor = NSColor.black
let normalLineLength = 50 // Number of chars per line with normalFont
let bigLineLength = 45 // Number of chars per line with bigFont
let glow = NSShadow()
glow.shadowColor = NSColor.red
glow.shadowBlurRadius = 2
glow.shadowOffset = CGSize(width: 2, height: 2)

// Available colors:
// black, blue, brown, cyan, darkGray, gray, green
// lightGray, magenta, orange, purple, red, white, yellow
// systemBlue , systemBrown, systemGray, systemGreen
// systemIndigo, systemOrange, systemPink , systemPurple
// systemRed, systemTeal , systemYellow


// Call a shell command
enum Shellstuff: Error { case unknown }
@discardableResult
private func shell(_ args: String) throws -> String {
    var outstr = ""
    let task = Process()
    task.launchPath = "/bin/sh"
    task.arguments = ["-c", args]
    let pipe = Pipe()
    task.standardOutput = pipe
    task.launch()
    let data = pipe.fileHandleForReading.readDataToEndOfFile()
    if let output = String(data: data, encoding: .utf8) {
        outstr = output as String
    } else {
        throw Shellstuff.unknown
    }
    task.waitUntilExit()
    return outstr
}


// First and last apps to show on current page.
var pageFirst = Int(0)
var pageLast = Int(0)
func resetPages () {
    pageFirst = Int(0)
    if appPids.count > 9 {
        pageLast = Int(9)
    } else {
        pageLast = (appPids.count - 1)
    }
}

func killAlterEgos () {
    let binInstancesCode = "ps -eo 'pid,command' | grep 'ActualForceQuit' | grep -v 'grep' | grep -v 'swift' | cut -d' ' -f1 | xargs"
    let swiftInstancesCode = "ps -eo 'pid,command' | grep 'module-name ActualForceQuit' | grep -v 'grep' | cut -d' ' -f1 | xargs"
    var binInstances = String()
    var swiftInstances = String()
    var cpuTime = String()
    do { binInstances = try shell(binInstancesCode) } catch { binInstances = "0" }
    do { swiftInstances = try shell(swiftInstancesCode) } catch { swiftInstances = "0" }
    let allInstancesDirty = binInstances + " " + swiftInstances
    let allInstances = allInstancesDirty.filter("0123456789 ".contains)
    //var allInstances = myText.trimmingCharacters(in: .whitespaces)
    let allInstancesArr = allInstances.split(separator: " ")
    //print(allInstances)
    for pid in allInstancesArr {
        let checkCpuTimeCode = "ps -p " + pid + " -o 'time' | tr -s ' ' | cut -d' ' -f2 | awk -F'[: ]+' '/:/ {t=$3+60*($2+60*$1); print t,$NF}'"
        do { cpuTime = try shell(checkCpuTimeCode) } catch { cpuTime = "0" }
        print(cpuTime)
    }
    // Lägg alla pids i en array och kör nåt i stil med följande, för att få cputime och spara den med minst (borde vara den som börjar köras nu):
    // ps -eo 'pid,time' | grep '^90174   '
    // Kanske omvandla till epic och sortera i nummerföljd...
}
killAlterEgos()

// How hard could it possibly be to keep focus? Sigh...
// The problem is that the window end up behind the new frontmost app when you quit the frontmost one.
// I've tried several tricks to solve this in swift but got none of them working reliably, so I threw in the towel
// and did it in this roundabout way with shell and applescript.
// The problem doesn't seem to occur if you run the app compiled, only when run as a script through the swift binary.
// Hence why we grep part of the ps command that's only there when we run it uncompiled.
// The window will 'disappear' for a second, then get yoinked back as soon as we get an idea of who we are.
// A problem with this method is that if you're running multiple instances of ActualForceQuit at the same
// time, you won't get the right window back but the oldest one. I decided I can live with that, since I
// can't see any reason for end users to do that.
func putMeFirst () {
    let lookAtMe = "ps -eo 'pid,command' | grep 'module-name ActualForceQuit' | grep -v 'grep' | xargs | cut -d' ' -f1 | tr -d '\n'"
    var iAm = String()
    do { iAm = try shell(lookAtMe) } catch { iAm = "nothing" }
    let theCaptainNow = "osascript -e 'tell application \"System Events\" to set frontmost of the first process whose unix id is " + iAm + " to true'"
    do { try shell(theCaptainNow) } catch {print("oh well") }
}


func tprint (txt: String, font: NSFont = normalFont!, clr: NSColor = textFGColor, glowing: String = "No") {
    var attrs = [
        NSAttributedString.Key.font: font,
        NSAttributedString.Key.foregroundColor: clr,
    ]
    if glowing == "yes" {
        attrs = [
            NSAttributedString.Key.font: font,
            NSAttributedString.Key.foregroundColor: clr,
            NSAttributedString.Key.shadow: glow
        ]
    }
    textView.textStorage!.append(NSMutableAttributedString(string: txt, attributes:attrs))
}


// print message
func printMessage (msg1: String = "", msg2: String = "", msg3: String = "", glowing: String = "no") {
    textView.string = ""
    let sMsg1 = msg1.prefix(normalLineLength)
    let sMsg2 = msg2.prefix(normalLineLength)
    let sMsg3 = msg3.prefix(normalLineLength)
    // If it's not obvious, the code below calculates the number of spaces needed to center the text.
    let space1 = String(repeating: " ", count: max((((normalLineLength) - sMsg1.count) / 2), 0))
    let space2 = String(repeating: " ", count: max((((normalLineLength) - sMsg2.count) / 2), 0))
    let space3 = String(repeating: " ", count: max((((normalLineLength) - sMsg3.count) / 2), 0))
    if glowing == "yes" {
        tprint(txt: "\n" + space1 + sMsg1 + "\n", font: normalFont!, clr: textFGColor, glowing: "yes")
        tprint(txt: space2 + sMsg2 + "\n", font: normalFont!, clr: textFGColor, glowing: "yes")
        tprint(txt: space3 + sMsg3 + "\n", font: normalFont!, clr: textFGColor, glowing: "yes")
    } else {
        tprint(txt: "\n" + space1 + sMsg1 + "\n", font: normalFont!, clr: textFGColor)
        tprint(txt: space2 + sMsg2 + "\n", font: normalFont!, clr: textFGColor)
        tprint(txt: space3 + sMsg3 + "\n", font: normalFont!, clr: textFGColor)
    }
}


// Make beautiful art
func printAppList () {
    tprint(txt: " ╭───────┬──────────────────────────────────────╮\n")
    tprint(txt: " │ ")
    tprint(txt: "PID", font: boldFont!)
    tprint(txt: "   │ ")
    tprint(txt: "APP", font: boldFont!)
    tprint(txt: "                                  │\n")
    tprint(txt: " ├───────┼──────────────────────────────────────┤\n")
    var index = Int(pageFirst)
    for appPid in appPids[pageFirst...pageLast] {
        let appName = appNames[index].prefix(37)
        tprint(txt: " │ ")
        tprint(txt: String(appPid).padding(toLength: 6, withPad: " ", startingAt: 0))
        tprint(txt: "│ ")
        tprint(txt: appName.padding(toLength: 37, withPad: " ", startingAt: 0))
        if appPid == appPids[pageLast] {
            tprint(txt: "│\n ╰───────┴──────────────────────────────────────╯\n")
        } else {
            tprint(txt: "│\n ├───────┼──────────────────────────────────────┤\n")
        }
        index += 1
    }
    tprint(txt: "            ")
    tprint(txt: "Showing " + String(pageFirst + 1) + "-" + String(pageLast + 1) + " out of " + String(appPids.count) + " apps")
}


// Show info
func printInfo (msg: String) {
    textView.string = "\n" + msg
}


// Get a list of open apps
var appPids = [Int]()
var appNames = [String]()
var appNamesAndPids = [String]()
var appsArr = [String]()
func getAppList () {
    appPids.removeAll()
    appNames.removeAll()
    appNamesAndPids.removeAll()
    appsArr.removeAll()
    var appsStr = ""
    if button4.state == NSControl.StateValue.on {
        do { appsStr = try shell("osascript -e 'tell application \"System Events\" to get {name, unix id} of every process'")
        } catch { appsStr = "{{\"Error\", \"Fetching\", \"Apps\"}, {1, 2, 3}}" }
    } else {
        do { appsStr = try shell("osascript -e 'tell application \"System Events\" to get {name, unix id} of (processes where background only is false)'")
        } catch { appsStr = "{{\"Error\", \"Fetching\", \"Apps\"}, {1, 2, 3}}" }
    }
    appsArr.append(contentsOf: appsStr.components(separatedBy: ", "))
    let half = appsArr.count / 2 // First half of the AS output is names, the second half is PID's.
    appNames.append(contentsOf: appsArr[0 ..< half])
    let appPidsDirty = appsArr[half ..< appsArr.count]
    var appPidsClean = [String]()
    for pid in appPidsDirty {
        let cleanPid = pid.filter("0123456789.".contains) // The last PID has a trailing '\n'.
        appPidsClean.append(cleanPid)
    }
    appPids = appPidsClean.map { Int($0)!}
    let zipped = zip(appNames, appPids)
    let appNamesAndPidsUnsorted = zipped.map { $0.0 + "," + String($0.1) }
    appNamesAndPids = appNamesAndPidsUnsorted.sorted{$0.caseInsensitiveCompare($1) == .orderedAscending}
    appPids.removeAll()
    appNames.removeAll()
    for appNameAndPid in appNamesAndPids {
        appNames.append(appNameAndPid.components(separatedBy: [","])[0])
        appPids.append(Int(appNameAndPid.components(separatedBy: [","])[1]) ?? 0)
    }
}


// Set up the application and window
let app = NSApplication.shared
app.setActivationPolicy(NSApplication.ActivationPolicy.accessory)
let window = NSWindow.init(
  contentRect: NSRect(x: 80, y: 70, width: 400, height: 500),
  styleMask:   [NSWindow.StyleMask.titled],
  backing:     NSWindow.BackingStoreType.buffered,
  defer:       false
)
window.center()
window.title = windowTitle
window.makeKeyAndOrderFront(nil)
window.orderFrontRegardless()
window.backgroundColor = windowBGColor


// Set up the UI
let textViewFrame = CGRect(x: 0, y: 73, width: 400, height: 433)
let textStorage = NSTextStorage()
var layoutManager = NSLayoutManager()
var textContainer = NSTextContainer(containerSize: textViewFrame.size)
textStorage.addLayoutManager(layoutManager)
layoutManager.addTextContainer(textContainer)
let textView = NSTextView(frame: textViewFrame, textContainer: textContainer)
let paragraphStyle = NSMutableParagraphStyle.default.mutableCopy() as! NSMutableParagraphStyle
paragraphStyle.lineBreakMode = .byCharWrapping
textView.defaultParagraphStyle = paragraphStyle
textView.isEditable = false
textView.isSelectable = true
textView.textColor = textFGColor
textView.font = normalFont!
textView.backgroundColor = textBGColor
textView.drawsBackground = true
window.contentView!.addSubview(textView)


getAppList()

resetPages()


var condolences:[[String]] = [
    ["X", "sleeps with the fishes."],
    ["X", "won't be missed."],
    ["See you in hell,", "X!"],
    ["We're better off without", "X."],
    ["Remind me, who's", "X again?"],
    ["X", "passed away silently."],
    ["Good. I always hated", "X."],
    ["Did anyone actually like", "X?"],
    ["X?", "Who cares!"],
    ["X", "was an asshat anyway."],
    ["Don't talk to me about", "X!"],
    ["Rest now X,", "the pain is over."],
    ["And so X", "has perished."],
    ["X", " died violently."],
    ["X?", "Never heard of 'em."],
    ["X", "has ceased to exist."],
    ["X", "died in a tragic accident."],
    ["X", "succumbed to fire."],
    ["X is dead.", "Praise be!"],
    ["X", "has departed."],
    ["Good riddance,", "X!"],
    ["X", "has expired."],
    ["X", "ran out of time."],
    ["X", "was bullied to death."],
    ["X", "is no more."],
    ["RIP X", ":D"],
    ["X", "mysteriously disappeared!"],
    ["X,", "forever forgotten."],
    ["You murdered", "X!"],
    ["X", "was slaughtered."],
    ["X", "is dead to me."],
    ["X", "was silently butchered."],
    ["X was executed.", "As in shot."],
    ["And STAY down,", "X!"],
    ["X", "was eliminated."],
    ["X", "was humanely dispatched."],
    ["X", "was silently massacred."],
    ["X", "is no more."],
    ["X", "is gone."],
    ["X", "died from water damage."],
    ["Nobody cries for", "X."],
    ["X", "won't disturb you no more."],
    ["X, as in", "the late X."],
    ["X", "has been exterminated."],
    ["X", "died by snu snu."],
    ["Want X gone?", "Consider it done."],
    ["X", "was wedgied to death."],
    ["Nobody weeps for", "X."],
    ["X", "has been dismissed."],
    ["X", "was shot in the back."],
    ["RIP in pieces,", "X!"],
    ["X", "went up in smoke."],
    ["X", "was eaten alive by ants."],
    ["X has been", "properly disposed of."],
    ["X", "will haunt your dreams."],
    ["X", "was left to the wolves."],
    ["X", "was brutally dismembered."],
    ["X", "is but a memory now."],
    ["Well, I didn't like", "X either."],
    ["Got X problems?", "Not anymore!"],
    ["X has been", "'taken care of'."]      
]


var procFlagStrings = [
    "8000000": "Proc in vfork",
    "4000000": "???",
    "2000000": "Process has vfork children",
    "1000000": "Process is being branch traced",
    "0800000": "Signal pending handling thread scheduled",
    "0400000": "Process is TBE",
    "0200000": "Process called reboot()",
    "0100000": "Blocked due to SIGTTOU or SIGTTIN",
    "0080000": "Kdebug tracing on for this process",
    "0040000": "Process has a wait() in progress",
    "0020000": "Process needs single-step fixup ???",
    "0010000": "Doing physical I/O / Tracing via file system",
    "0008000": "Another flag to prevent swap out",
    "0004000": "Process called exec",
    "0002000": "Working on exiting",
    "0001000": "Debugging process has waited for child",
    "0000800": "Debugged process being traced",
    "0000400": "Timing out during sleep",
    "0000200": "System proc: no sigs, stats or swapping",
    "0000100": "Had set id privileges since last exec",
    "0000080": "Process was stopped and continued",
    "0000040": "Selecting; wakeup/waiting danger",
    "0000020": "Has started profiling",
    "0000010": "Parent is waiting for child to exec/exit",
    "0000008": "No SIGCHLD when children stop",
    "0000004": "Process is LP64",
    "0000002": "Has a controlling terminal",
    "0000001": "Process may hold a POSIX advisory lock"
]


func getFlagStrings (raw: String) -> String {
    // Converts flag output from ps into strings (from sys/proc.h)
    var flagStrings = String("\n")
    if var numbInt = Int(raw) {
        for (k,v) in Array(procFlagStrings).sorted(by: {$0.0 > $1.0}) {
            let valInt = Int(k)!
            if (numbInt - valInt) >= 0 {
                flagStrings = flagStrings + "'" + v + "'\n"
                numbInt -= valInt
            }
        }
    } else {
        flagStrings += "Error getting flags\n"
    }
    return flagStrings
}


var procStatesA = [
    "I": "The process is idle (sleeping for longer than about 20 seconds).",
    "R": "The process is runnable.",
    "S": "The process is sleeping for less than about 20 seconds.",
    "T": "The process is stopped.",
    "U": "The process is in uninterruptible wait.",
    "Z": "The process is dead (a ``zombie'').",
]

var procStatesB = [
    "+": "The process is in the foreground process group of its control terminal.",
    "<": "The process has raised CPU scheduling priority.",
    ">": "The process has specified a soft limit on memory requirements and is exceeding it.",
    "A": "the process has asked for random page replacement.",
    "E": "The process is trying to exit.",
    "L": "The process has pages locked in core (for example, for raw I/O).",
    "N": "The process has reduced CPU scheduling priority (see setpriority(2)).",
    "S": "The process has asked for FIFO page replacement",
    "s": "The process is a session leader.",
    "V": "The process is suspended during a vfork(2).",
    "W": "The process is swapped out.",
    "X": "The process is being traced or debugged."
]

func getStateStrings (raw: String) -> String {
    // Converts state output from ps into strings
    var stateStrings = "\n"
    var num = Int(0)
    for char in raw {
        let cstr = String(char)
        if num == 0 {
            stateStrings = stateStrings + "'" + procStatesA[cstr]! + "'\n"
        } else {
            stateStrings = stateStrings + "'" + procStatesB[cstr]! + "'\n"
        }
        num += 1
    }
    return stateStrings
}


// Create targets
class TargetButton1 { // kill -9
  @objc func onClick () {
    button1.isEnabled = true 
    button4.isEnabled = true 
    button5.isEnabled = true 
    button6.isEnabled = true 
    button7.isEnabled = true 
    textView.font = normalFont!
    let string = textView.attributedString().string
    let selectedRange = textView.selectedRange()
    let startIndex = string.index(string.startIndex, offsetBy: selectedRange.lowerBound)
    let endIndex = string.index(string.startIndex, offsetBy: selectedRange.upperBound)
    let substring = textView.attributedString().string[startIndex..<endIndex]
    let selectedString = String(substring)
    if selectedString.isEmpty {
        if killCurrent {
            let murderCommand = "kill -9 " + currentAppPid
            do { try shell(murderCommand)
            } catch { print("murder error") }
            let randNum = Int.random(in: 0..<condolences.count)
            let msg1 = condolences[randNum][0].replacingOccurrences(of: "X", with: currentAppName)
            let msg2 = condolences[randNum][1].replacingOccurrences(of: "X", with: currentAppName)
            printMessage(msg1: msg1, msg2: msg2, glowing: "yes")
            printAppList()
            //window.makeKeyAndOrderFront(nil)
            //window.orderFrontRegardless()
            putMeFirst()   
        } else {
            printMessage(msg2: "Hint: Select a number!")
            printAppList()
        }
    } else {
        if let selPid = Int(selectedString) {
            if appPids.contains(selPid) {
                if let index = appPids.firstIndex(of: selPid) {
                    let murderCommand = "kill -9 " + String(selPid)
                    do { try shell(murderCommand)
                    } catch { print("murder error") }
                    let appName = appNames[index]
                    let randNum = Int.random(in: 0..<condolences.count)
                    let msg1 = condolences[randNum][0].replacingOccurrences(of: "X", with: appName)
                    let msg2 = condolences[randNum][1].replacingOccurrences(of: "X", with: appName)
                    printMessage(msg1: msg1, msg2: msg2, glowing: "yes")
                    printAppList()
                    putMeFirst()
                }
            } else {
                printMessage(msg2: "That's not a valid PID.")
                printAppList()
            }
        } else {
            printMessage(msg2: "Hint: Select a number!")
            printAppList()
        }
    }
    killCurrent = false
  }
}
let targetButton1 = TargetButton1()


class TargetButton2 { //Refresh
  @objc func onClick () {
    killCurrent = false
    button1.isEnabled = true 
    button4.isEnabled = true 
    button5.isEnabled = true 
    button6.isEnabled = true 
    button7.isEnabled = true 
    textView.font = normalFont!
    textView.string = "Refreshing..."
    getAppList()
    resetPages()
    printMessage(msg2: "Double-click a PID and press 'kill -9' to quit")
    printAppList()
  }
}
let targetButton2 = TargetButton2()


class TargetButton3 { // Leave
  @objc func onClick () {
    exit(0)
  }
}
let targetButton3 = TargetButton3()


class TargetButton4 { // Show background apps
  @objc func onClick () {
    killCurrent = false
    textView.font = normalFont!
    if button4.state == NSControl.StateValue.on {
        button4.title = "hide bg apps"
        if let mutableAttributedTitle = button4.attributedTitle.mutableCopy() as? NSMutableAttributedString {
            mutableAttributedTitle.addAttribute(.foregroundColor, value: buttonTextColor, range: NSRange(location: 0, length: mutableAttributedTitle.length))
            button4.attributedTitle = mutableAttributedTitle
        }
        targetButton2.onClick()
    } else {
        button4.title = "show bg apps"
        if let mutableAttributedTitle = button4.attributedTitle.mutableCopy() as? NSMutableAttributedString {
            mutableAttributedTitle.addAttribute(.foregroundColor, value: buttonTextColor, range: NSRange(location: 0, length: mutableAttributedTitle.length))
            button4.attributedTitle = mutableAttributedTitle
        }
        targetButton2.onClick()
    }
  }
}
let targetButton4 = TargetButton4()


class TargetButton5 { // Previous
  @objc func onClick () {
    killCurrent = false
    textView.font = normalFont!
    //getAppList()
    if pageFirst >= 10 {
        if ((pageLast + 1) % 10 == 0) {
            pageFirst = pageFirst - 10
            pageLast = pageLast - 10
        } else {
            let lastdiv = Double(pageLast / 10)
            pageLast = Int((floor(lastdiv) * 10) - 1)
            pageFirst -= 10
        }
    } else {
        pageFirst = 0
    }
    printMessage(msg2: "Double-click a PID and press 'kill -9' to quit")
    printAppList()
  }
}
let targetButton5 = TargetButton5()


class TargetButton6 { // Next
  @objc func onClick () {
    killCurrent = false
    textView.font = normalFont!
    //getAppList()
    if (pageLast + 10) >= appPids.count {
        pageLast = appPids.count - 1
        let divi = Double(pageLast / 10)
        pageFirst = Int((floor(divi) * 10))
    } else {
        pageFirst += 10
        pageLast += 10
    }
    printMessage(msg2: "Double-click a PID and press 'kill -9' to quit")
    printAppList()
  }
}
let targetButton6 = TargetButton6()


class TargetButton7 { // Info Button
  @objc func onClick () {
    killCurrent = false
    button1.isEnabled = false
    button4.isEnabled = false
    button5.isEnabled = false
    button6.isEnabled = false
    button7.isEnabled = false
    let string = textView.attributedString().string
    let selectedRange = textView.selectedRange()
    let startIndex = string.index(string.startIndex, offsetBy: selectedRange.lowerBound)
    let endIndex = string.index(string.startIndex, offsetBy: selectedRange.upperBound)
    let substring = textView.attributedString().string[startIndex..<endIndex]
    let selectedString = String(substring)
    if let selPid = Int(selectedString) {
        if appPids.contains(selPid) {
            textView.font = smallFont
            if let index = appPids.firstIndex(of: selPid) {
                button1.isEnabled = true
                killCurrent = true // kill button checks this if nothing is selected and kills currentAppPid if true
                currentAppPid = String(selPid)
                currentAppName = appNames[index] // and this is what kill button says it killed
                let appName = appNames[index]
                let procFileCmd = "lsof -p " + String(selPid) + " | grep txt | head -n 1 | tr -s ' ' | cut -d' ' -f9- | tr -d '\n'"
                let procUserCmd = "ps -eo 'pid,user' | grep '" + String(selPid) +       "' | grep -v 'grep' | xargs | cut -d' ' -f2 | tr -d '\n'"
                let procTimeCmd = "ps -eo 'pid,start' | grep '" + String(selPid) +      "' | grep -v 'grep' | xargs | cut -d' ' -f2 | tr -d '\n'"
                let procFlagsCmd = "ps -eo 'pid,flags' | grep '" + String(selPid) +     "' | grep -v 'grep' | xargs | cut -d' ' -f2 | tr -d '\n'"
                let procCommandCmd = "ps -eo 'pid,command' | grep '" + String(selPid) + "' | grep -v 'grep' | xargs | cut -d' ' -f2- | tr -d '\n'"
                let procStateCmd = "ps -eo 'pid,stat' | grep '" + String(selPid) +      "' | grep -v 'grep' | xargs | cut -d' ' -f2 | tr -d '\n'"
                var procFile = String()
                var procUser = String()
                var procTime = String()
                var procFlags = String()
                var procCommand = String()
                var procState = String()
                do { procFile = try shell(procFileCmd) } catch { procFile = "error" }
                do { procUser = try shell(procUserCmd) } catch { procUser = "error"}
                do { procTime = try shell(procTimeCmd) } catch { procTime = "error"}
                do { procFlags = try shell(procFlagsCmd) } catch { procFlags = "error"}
                let procFlagsStr = getFlagStrings(raw: procFlags)
                do { procCommand = try shell(procCommandCmd) } catch { procCommand = "error"}
                do { procState = try shell(procStateCmd) } catch { procState = "error"}
                let procStateStr = getStateStrings(raw: procState)
                var msg = "Process ID " + String(selPid) + ": " + appName
                msg += "\n----------------------------------------------------------------\n"
                msg += "\nStarted by user " + procUser + " at " + procTime + "\n\n"
                msg += "State: " + procStateStr + "\n"
                msg += "Flags: " + procFlagsStr + "\n"
                msg += "File path: \n" + procFile + "\n\n"
                msg += "Started with command: \n" + procCommand
                printInfo(msg: msg)
            }
        } else {
            button1.isEnabled = true 
            button4.isEnabled = true 
            button5.isEnabled = true 
            button6.isEnabled = true 
            button7.isEnabled = true 
            printMessage(msg2: "That's not a valid PID.")
            printAppList()
        }
    } else {
        //var msg = "\n"
        let urlString1 = NSMutableAttributedString(string: "j@qr.ax")
        let urlString2 = NSMutableAttributedString(string: "https://me.qr.ax/projects/afq.html")
        let urlString3 = NSMutableAttributedString(string: "https://gitlab.com/johsoderi/actualforcequit")
        let range1 = NSRange(location: 0, length: 7)
        let range2 = NSRange(location: 0, length: 34)
        let range3 = NSRange(location: 0, length: 44)
        let url1 = URL(string: "mailto:j@qr.ax")!
        let url2 = URL(string: "https://me.qr.ax/projects/afq.html")!
        let url3 = URL(string: "https://gitlab.com/johsoderi/actualforcequit")!
        urlString1.setAttributes([.link: url1], range: range1)
        urlString1.addAttribute(NSMutableAttributedString.Key.font, value: normalFont!, range: range1)
        urlString2.setAttributes([.link: url2], range: range2)
        urlString2.addAttribute(NSMutableAttributedString.Key.font, value: normalFont!, range: range2)
        urlString3.setAttributes([.link: url3], range: range3)
        urlString3.addAttribute(NSMutableAttributedString.Key.font, value: normalFont!, range: range3)
        textView.linkTextAttributes = [
            // Adding the font here did nothing for some reason...
            .underlineStyle: NSUnderlineStyle.single.rawValue,
            .foregroundColor: textFGColor,
            .shadow: glow
        ]
        textView.string = "\n"
        tprint(txt: " ╔══════════════════════════════════════════════╗ \n", glowing: "yes")
        tprint(txt: " ║                                              ║ \n", glowing: "yes")
        tprint(txt: " ║         ActualForceQuit by johsoderi         ║ \n", glowing: "yes")
        tprint(txt: " ║                                              ║ \n", glowing: "yes")
        tprint(txt: " ║               " + version + "                ║ \n", glowing: "yes")
        tprint(txt: " ║                                              ║ \n", glowing: "yes")
        tprint(txt: " ║    Tested on Big Sur 11.4 w/ Swift v5.3.2    ║ \n", glowing: "yes")
        tprint(txt: " ║                                              ║ \n", glowing: "yes")
        tprint(txt: " ║                   ", glowing: "yes")
        textView.textStorage!.append(urlString1)
        tprint(txt: "                    ║ \n", glowing: "yes")
        tprint(txt: " ║      ", glowing: "yes")
        textView.textStorage!.append(urlString2)
        tprint(txt: "      ║ \n", glowing: "yes")
        tprint(txt: " ║ ", glowing: "yes")
        textView.textStorage!.append(urlString3)
        tprint(txt: " ║ \n", glowing: "yes")
        tprint(txt: " ║                                              ║ \n", glowing: "yes")
        tprint(txt: " ╚══════════════════════════════════════════════╝ \n\n\n ", glowing: "yes")
        tprint(txt: "           For info on a process:             \n\n", font: bigFont!)
        tprint(txt: "    Refresh, select a PID and press 'i' again.   ")
    }
  }
}
let targetButton7 = TargetButton7()


// Sprinkle some buttons
let button1 = NSButton.init(
  title:  "kill -9",
  target: targetButton1,
  action: #selector(TargetButton1.onClick)
)
button1.frame = NSRect(x:5, y:4, width:220, height:30)
button1.bezelStyle =  NSButton.BezelStyle.smallSquare
button1.font = buttonTextFont!
if let mutableAttributedTitle = button1.attributedTitle.mutableCopy() as? NSMutableAttributedString {
    mutableAttributedTitle.addAttribute(.foregroundColor, value: buttonTextColor, range: NSRange(location: 0, length: mutableAttributedTitle.length))
    //mutableAttributedTitle.addAttribute(.shadow, value: glow, range: NSRange(location: 0, length: mutableAttributedTitle.length))
    button1.attributedTitle = mutableAttributedTitle
}
window.contentView!.addSubview(button1)


let button2 = NSButton.init(
  title:  "refresh",
  target: targetButton2,
  action: #selector(TargetButton2.onClick)
)
button2.frame = NSRect(x:5, y:36, width:84, height:30)
button2.bezelStyle =  NSButton.BezelStyle.smallSquare
button2.font = buttonTextFont!
if let mutableAttributedTitle = button2.attributedTitle.mutableCopy() as? NSMutableAttributedString {
    mutableAttributedTitle.addAttribute(.foregroundColor, value: buttonTextColor, range: NSRange(location: 0, length: mutableAttributedTitle.length))
    button2.attributedTitle = mutableAttributedTitle
}
window.contentView!.addSubview(button2)


let button3 = NSButton.init(
  title:  "done",
  target: targetButton3,
  action: #selector(TargetButton3.onClick)
)
button3.frame = NSRect(x:269, y:4, width:126, height:30)
button3.bezelStyle =  NSButton.BezelStyle.smallSquare
button3.font = buttonTextFont!
if let mutableAttributedTitle = button3.attributedTitle.mutableCopy() as? NSMutableAttributedString {
    mutableAttributedTitle.addAttribute(.foregroundColor, value: buttonTextColor, range: NSRange(location: 0, length: mutableAttributedTitle.length))
    button3.attributedTitle = mutableAttributedTitle
}
window.contentView!.addSubview(button3)


let button4 = NSButton.init(
  title:  "show bg apps",
  target: targetButton4,
  action: #selector(TargetButton4.onClick)
)
button4.frame = NSRect(x:269, y:36, width:126, height:30)
button4.bezelStyle =  NSButton.BezelStyle.smallSquare
button4.font = buttonTextFont!
if let mutableAttributedTitle = button4.attributedTitle.mutableCopy() as? NSMutableAttributedString {
    mutableAttributedTitle.addAttribute(.foregroundColor, value: buttonTextColor, range: NSRange(location: 0, length: mutableAttributedTitle.length))
    button4.attributedTitle = mutableAttributedTitle
}
window.contentView!.addSubview(button4)


let button5 = NSButton.init(
  title:  "<<",
  target: targetButton5,
  action: #selector(TargetButton5.onClick)
)
button5.frame = NSRect(x:93, y:36, width:84, height:30)
button5.bezelStyle =  NSButton.BezelStyle.smallSquare
button5.font = buttonTextFont!
if let mutableAttributedTitle = button5.attributedTitle.mutableCopy() as? NSMutableAttributedString {
    mutableAttributedTitle.addAttribute(.foregroundColor, value: buttonTextColor, range: NSRange(location: 0, length: mutableAttributedTitle.length))
    button5.attributedTitle = mutableAttributedTitle
}
window.contentView!.addSubview(button5)


let button6 = NSButton.init(
  title:  ">>",
  target: targetButton6,
  action: #selector(TargetButton6.onClick)
)
button6.frame = NSRect(x:181, y:36, width:84, height:30)
button6.bezelStyle =  NSButton.BezelStyle.smallSquare
button6.font = buttonTextFont!
if let mutableAttributedTitle = button6.attributedTitle.mutableCopy() as? NSMutableAttributedString {
    mutableAttributedTitle.addAttribute(.foregroundColor, value: buttonTextColor, range: NSRange(location: 0, length: mutableAttributedTitle.length))
    button6.attributedTitle = mutableAttributedTitle
}
window.contentView!.addSubview(button6)


let button7 = NSButton.init(
  title:  "i",
  target: targetButton7,
  action: #selector(TargetButton7.onClick)
)
button7.frame = NSRect(x:229, y:4, width:36, height:30)
button7.bezelStyle =  NSButton.BezelStyle.smallSquare
button7.font = buttonTextFont!
if let mutableAttributedTitle = button7.attributedTitle.mutableCopy() as? NSMutableAttributedString {
    mutableAttributedTitle.addAttribute(.foregroundColor, value: buttonTextColor, range: NSRange(location: 0, length: mutableAttributedTitle.length))
    button7.attributedTitle = mutableAttributedTitle
}
window.contentView!.addSubview(button7)


var currentAppName = String()
do { currentAppName = try shell("""
osascript -e 'tell application \"System Events\"' \
-e 'get name of first application process whose frontmost is true and visible is true' \
-e 'end tell' | tr -d '\n'
""")
} catch { currentAppName = "ErrorGettingName" }


var currentAppPid = String()
do { currentAppPid = try shell("""
osascript -e 'tell application \"System Events\"' \
-e 'get unix id of first application process whose frontmost is true and visible is true' \
-e 'end tell' | tr -d '\n'
""")
} catch { currentAppPid = "0000" }

var killCurrent = true
let truncedName = currentAppName.prefix(bigLineLength)
let space = String(repeating: " ", count: max((((bigLineLength) - truncedName.count) / 2), 0))
textView.string = "\n"
tprint(txt: "   Press 'kill -9' now to force quit PID " + currentAppPid + ": \n")
tprint(txt: space + truncedName + "\n", font: bigFont!)
tprint(txt: "    Or double-click any PID, then 'kill -9' it.\n")

printAppList()

app.activate(ignoringOtherApps: true)
app.run()
putMeFirst()

