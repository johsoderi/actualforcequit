![example](https://me.qr.ax/random/afq_1_13_2.png)

## How to quit
Run the script interpreted with `$ swift ActualForceQuit.swift` or compile it with `$ swiftc ActualForceQuit.swift` to get a speedier binary.


You can bind the script/binary to a keyboard shortcut for convenience, I use BetterTouchTool for this.


If you don't have the swift binary, install "XCode Command Line Tools" with `$ sudo xcode-select --install`.


More info here: [https://me.qr.ax/projects/afq.html](https://me.qr.ax/projects/afq.html)
